FROM alpine:3.13.2 AS builder
ARG THTTPD_VERSION=2.29
RUN apk add gcc musl-dev make && \
  wget http://www.acme.com/software/thttpd/thttpd-${THTTPD_VERSION}.tar.gz \
  && tar xzf thttpd-${THTTPD_VERSION}.tar.gz \
  && mv /thttpd-${THTTPD_VERSION} /thttpd && \
  cd /thttpd \
  && ./configure \
  && make CCOPT='-O2 -s -static' thttpd && \
  adduser -D www

FROM scratch
EXPOSE 80
COPY --from=builder /etc/passwd /etc/passwd
COPY --from=builder /thttpd/thttpd /
USER www
WORKDIR /home/www
ENTRYPOINT ["/thttpd", "-D", "-h", "0.0.0.0", "-u", "www", "-l", "-", "-M", "360"]
